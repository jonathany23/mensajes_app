package org.jonathany23.persist;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
    public Connection getConnection(){
        Connection connection = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app", "root", "root1234");
            if (connection != null){
                System.out.println("Conexion Exitosa");
            }
        } catch (Exception e){
            System.err.println(e);
            e.printStackTrace();
        }
        return connection;
    }
}
